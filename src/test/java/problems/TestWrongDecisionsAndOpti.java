package problems;

import main.Head;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.xcsp.common.Utilities;

import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static problems.UtilityForTests.runResolution;

@RunWith(Parameterized.class)
public class TestWrongDecisionsAndOpti {

	static Collection<Object[]> collection = new LinkedList<>();

	static void add(Object instance, int nWrongDecisions, int bound, String pars) {
		pars += " -ev";
		URL url = Head.class.getResource(instance + ".xml.lzma");
		Utilities.control(url != null, "not found: " + instance + ".xml.lzma");
		collection.add(new Object[] { url.getPath() + " " + pars, nWrongDecisions, bound });
	}

	static void add(String instance, int nWrongDecisions, int bound) {
		add(instance, nWrongDecisions, bound, "");
	}

	@Parameters(name = "{index}: {0} has {1} wrong decisions")
	public static Collection<Object[]> data() {

		// ST
		add("/cop/StillLife-7-7", 132235, 28, "-r_srp=99999999 -r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=8 -defMDuels=MDuelsMax");
		add("/cop/StillLife-7-7", 136747, 28, "-r_srp=99999999 -r_luby=True -varh=HVCSingleTournament -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=ESBReward -bandit=NullPolicy -r_rrp=99999999 -nSubRuns=1 -defMDuels=MDuelsMax");

		// Safe strat for ABD
		add("/cop/StillLife-7-7", 178298, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=2 -extCutoff=1");

		// extended cutoff
		add("/cop/StillLife-7-7", 207533, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=1.1 -extCutoff=1");
		add("/cop/StillLife-7-7", 217424, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=1.1 -extCutoff=2");

		// exp gap factors
		add("/cop/StillLife-7-7", 107034, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=1.1");
		add("/cop/StillLife-7-7", 119617, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=1.5");
		add("/cop/StillLife-7-7", 273133, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=4");
		add("/cop/StillLife-7-7", 126200, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=8");

		// prev gap factors
		add("/cop/StillLife-7-7", 145101, 28, "-positive=str2 -exd=false -gap=PreviousGap -fpg=1.1");
		add("/cop/StillLife-7-7", 195337, 28, "-positive=str2 -exd=false -gap=PreviousGap -fpg=1.5");
		add("/cop/StillLife-7-7", 265931, 28, "-positive=str2 -exd=false -gap=PreviousGap -fpg=4");
		add("/cop/StillLife-7-7", 237609, 28, "-positive=str2 -exd=false -gap=PreviousGap -fpg=8");

		// All the gap sequences
		add("/cop/StillLife-7-7", 106194, 28, "-positive=str2 -exd=false -gap=PreviousGap -fpg=2");
		add("/cop/StillLife-7-7", 228512, 28, "-positive=str2 -exd=false -gap=RExpGap");
		add("/cop/StillLife-7-7", 163982, 28, "-positive=str2 -exd=false -gap=LubyGap");
		add("/cop/StillLife-7-7", 201313, 28, "-positive=str2 -exd=false -gap=ExponentialGap -expoGapFactor=2");
		add("/cop/StillLife-7-7", 123984, 28, "-varh=HVCAtRun -varhSet=CACD,CHS,DDEGONDOM,ACTIVITY,IMPACT -reward=NPTSReward -bandit=Moss");

		return collection;
	}

	@Parameter(0)
	public String args;

	@Parameter(1)
	public int nWrongDecisions;

	@Parameter(2)
	public int bound;

	@Test
	public void test() throws InterruptedException {
		assertEquals(nWrongDecisions, runResolution(args).solver.stats.nWrongDecisions);
		assertEquals(bound, runResolution(args).solver.solRecorder.bestBound);
	}
}
