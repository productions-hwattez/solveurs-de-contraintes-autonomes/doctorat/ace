package problems;

import main.Head;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.xcsp.common.Utilities;

import java.math.BigInteger;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static problems.UtilityForTests.runResolution;

@RunWith(Parameterized.class)
public class TestRewards {

	static Collection<Object[]> collection = new LinkedList<>();

	static void add(Object instance, BigInteger pts, String pars) {
		pars += " -ev";
		URL url = Head.class.getResource(instance + ".xml.lzma");
		Utilities.control(url != null, "not found: " + instance + ".xml.lzma");
		collection.add(new Object[] { url.getPath() + " " + pars, pts });
	}

	static void add(String instance, BigInteger pts) {
		add(instance, pts, "");
	}

	@Parameters(name = "{index}: {0} has {1} wrong decisions")
	public static Collection<Object[]> data() {

		add("/csp/Rlfap-scen-11-f06", new BigInteger("46755865772192974432596642683333147253800687346137869763668819790965046940582243051219321976249226015417336998932038798339653273497359345040350595163297224713724635342450572921493293705611541738644546370154018350769081010352366786435027290193703891256011229297094305762338507302505830303868058812458114954333464399012585525650441289269922157729241534535556680531880214460652362740937524237796102581315158097172028163068737150899165346314573319045602372411404192865947508478526268876676554956767662479301937030792621350048448779576323470873100794387981211336871705296250181254582146092513775269613621299176496174087446604595114644429474626090233260619036798882142069725203753208734935114739008646635297126614174175023908402462495095463393393199265899265949528470590667329343711487972047417793124036364030091839504584326387964985674939603113138585490431704019489476271586759761684215493629665972760588676056848493593030598904809786830630957384136668262180460369439830059320569888950869644484487794502526153482223719497154232193958599093911552"), "-varh=WdegOnDom");
		add("/csp/Crossword-lex-vg-5-6", new BigInteger("8274193955738455530371411479823481474656"), "-varh=WdegOnDom");

		return collection;
	}

	@Parameter(0)
	public String args;

	@Parameter(1)
	public BigInteger pts;

	@Test
	public void test() throws InterruptedException {
		assertEquals(pts, runResolution(args).solver.stats.cumulatedPruneTreeSizesNG);
	}
}
