package bandit;

import solver.Solver;

import java.util.Arrays;
import java.util.Random;

public abstract class BanditPolicy {
    protected final int k;
    protected final Random rand;

    protected int totalCalls = 0;
    protected final int[] armCallNumber;
    protected final double[] armMean;

    public BanditPolicy(Solver solver, int k) {
        this.k = k;
        this.rand = new Random(solver.head.control.general.seed);

        armCallNumber = new int[k];
        armMean = new double[k];
    }

    public int[] getArmCallNumber() {
        return armCallNumber;
    }

    public double[] getArmMean() {
        return armMean;
    }

    public abstract int nextAction();

    public void update(int i, double reward) {
        this.totalCalls++;
        this.armCallNumber[i]++;
        this.armMean[i] = (this.armMean[i] * (this.armCallNumber[i] - 1) + reward) / this.armCallNumber[i];
        this.subUpdate(i, reward);
    }

    protected abstract void subUpdate(int i, double reward);

    @Override
    public String toString() {
        return Arrays.toString(armCallNumber);
    }

}
