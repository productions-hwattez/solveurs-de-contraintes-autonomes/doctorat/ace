package bandit;

import solver.Solver;

public class Ucb extends BanditPolicy {

    public Ucb(Solver solver, int k) {
        super(solver, k);
    }

    @Override
    public int nextAction() {
        if (this.totalCalls < this.k) {
            return this.totalCalls;
        }

        int maxValueIndex = -1;
        double valuesMax = -Double.MAX_VALUE;

        for (int i = 0; i < armMean.length; i++) {

            double val = this.getUcbValue(i);

            if (valuesMax < val) {
                valuesMax = val;
                maxValueIndex = i;
            }
        }

        return maxValueIndex;
    }

    private double getUcbValue(int i) {
        int t = Math.max(1, this.totalCalls);
        int s = Math.max(1, this.armCallNumber[i]);
        double ucb = Math.sqrt((8 * Math.log(t)) / s);
        return armMean[i] + ucb;
    }

    @Override
    protected void subUpdate(int i, double reward) {
        // Nothing to do
    }
}
