package bandit;

import cern.jet.random.Beta;
import cern.jet.random.engine.MersenneTwister;
import solver.Solver;

public class TS extends BanditPolicy {

    private final Beta betaFunction;

    public TS(Solver solver, int k) {
        super(solver, k);
        this.betaFunction = new Beta(1, 1, new MersenneTwister((int) solver.head.control.general.seed));
    }

    @Override
    public int nextAction() {
        if (this.totalCalls < this.k) {
            return this.totalCalls;
        }

        int maxValueIndex = -1;
        double valuesMax = -Double.MAX_VALUE;

        for (int i = 0; i < armMean.length; i++) {

            double val = this.getTSValue(i);

            if (valuesMax < val) {
                valuesMax = val;
                maxValueIndex = i;
            }
        }

        return maxValueIndex;
    }

    private double getTSValue(int i) {
        double val = armMean[i];
        double alpha = 1 + armCallNumber[i] * val;
        double beta = 1 + armCallNumber[i] * (1 - val);

        return betaFunction.nextDouble(alpha, beta);
    }

    @Override
    protected void subUpdate(int i, double reward) {
        // Nothing to do
    }
}
