package bandit;

import solver.Solver;

public class BiasedPolicy extends BanditPolicy {

    private final double[] bias;

    public BiasedPolicy(Solver solver, int k) {
        super(solver, k);
        this.bias = new double[k];

        int i=0;
        for (String b : solver.head.control.varh.biasedDistrib.split(",")) {
            this.bias[i++] = Double.parseDouble(b);
        }
    }

    @Override
    public int nextAction() {
        double d = this.rand.nextDouble();

        for (int i=0; i<this.bias.length; i++) {
            d -= this.bias[i];
            if (d < 0.) {
                return i;
            }
        }

        return this.bias.length - 1;
    }

    @Override
    protected void subUpdate(int i, double reward) {

    }
}
