package bandit.reward;

import solver.Statistics;

public class ESBReward implements RewardStrategy {

    private Statistics stats;

    public ESBReward(Statistics stats) {
        this.stats = stats;
    }

    @Override
    public double getReward() {
        return Math.log((double) stats.solver.restarter.offset) / Math.log(2) / this.stats.subtreeSize.bitLength();
    }

    @Override
    public void afterRun() {
    }

}

