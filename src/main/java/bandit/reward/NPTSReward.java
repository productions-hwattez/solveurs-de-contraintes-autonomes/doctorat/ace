package bandit.reward;

import java.math.BigInteger;

import solver.Statistics;

public class NPTSReward implements RewardStrategy {

    private Statistics stats;
    private BigInteger previousPTS;

    public NPTSReward(Statistics stats) {
        this.stats = stats;
        this.previousPTS = this.stats.cumulatedPruneTreeSizesNG;
    }

    @Override
    public double getReward() {
        BigInteger diff = this.stats.cumulatedPruneTreeSizesNG.subtract(this.previousPTS);
        this.previousPTS = this.stats.cumulatedPruneTreeSizesNG;
        return ((double) diff.bitLength()) / this.stats.fullTreeSize.bitLength();
    }

    @Override
    public void afterRun() {
    }

}

