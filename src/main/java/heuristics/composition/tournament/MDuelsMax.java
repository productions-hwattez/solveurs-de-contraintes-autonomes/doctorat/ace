package heuristics.composition.tournament;

import heuristics.composition.VarHManager;

public class MDuelsMax extends MDuels {

    public MDuelsMax(int mMatchs) {
        super(mMatchs);
    }

    @Override
    protected VarHManager getTheBestCandidate() {
        double r1 = 0, r2 = 0;

        for (int i=0; i<this.mMatchs; i++) {
            r1 = Math.max(this.matchs[i], r1);
            r2 = Math.max(this.matchs[i+this.mMatchs], r2);
        }

        return r1 > r2 ? this.candidates.get(0) : this.candidates.get(1);
    }
}
