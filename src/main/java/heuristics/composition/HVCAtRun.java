package heuristics.composition;

import constraints.Constraint;
import solver.Solver;
import variables.Variable;

public class HVCAtRun extends HeuristicVariableComposition {

    public HVCAtRun(Solver solver, boolean antiHeuristic) {
        super(solver, antiHeuristic);
    }

    @Override
    public void internalBeforeRun() {
        this.selectNextVarH();
        this.currentVarH.beforeRun();
    }

    private void selectNextVarH() {
        this.lastAction = this.bandit.nextAction();
        this.currentVarHManager = varHManagers.get(this.lastAction);
        this.currentVarH = this.currentVarHManager.getVarH();
        this.currentVarHManager.select();
    }

    @Override
    public void afterRun() {
        this.reward.afterRun();
        this.bandit.update(this.lastAction, this.reward.getReward());
        this.currentVarH.afterRun();
    }

    @Override
    public void whenWipeout(Constraint c, Variable x) {
        this.currentVarH.whenWipeout(c, x);
    }

    @Override
    public void beforeAssignment(Variable x, int a) {
        this.currentVarH.beforeAssignment(x, a);
    }

    @Override
    public void afterAssignment(Variable x, int a) {
        this.currentVarH.afterAssignment(x, a);
    }

    @Override
    public void afterUnassignment(Variable x) {
        this.currentVarH.afterUnassignment(x);
    }

    @Override
    public double scoreOf(Variable x) {
        return this.currentVarH.scoreOf(x);
    }

    @Override
    public Variable bestUnpriorityVar() {
        return this.currentVarH.bestUnpriorityVar();
    }

    @Override
    public String toString() {
        return this.currentVarHManager.toString();
    }

}
