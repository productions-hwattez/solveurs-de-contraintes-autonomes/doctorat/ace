package heuristics.composition;

import constraints.Constraint;
import solver.Solver;
import utility.Enums;
import variables.Variable;

public class GrimesSamplingHVCAtRun extends HeuristicVariableComposition {

    private static final int N_RUNS_FOR_SAMPLING = 40;

    private final Enums.ERestartsMeasure initialRestartMeasure;
    private int currentCutoff = 0;
    private boolean samplingMode = true;

    public GrimesSamplingHVCAtRun(Solver solver, boolean antiHeuristic) {
        super(solver, antiHeuristic);

        this.initialRestartMeasure = this.solver.head.control.restarts.measure;
    }

    @Override
    public void internalBeforeRun() {
        this.selectNextVarH();

        for (VarHManager m : this.varHManagers) {
            m.getVarH().beforeRun();
        }
    }

    private void selectNextVarH() {
        this.lastAction = this.sampling() ? 1 : 0;
        this.currentVarHManager = varHManagers.get(this.lastAction);
        this.currentVarH = this.currentVarHManager.getVarH();
        this.currentVarHManager.select();
    }

    private boolean sampling() {
        if (this.samplingMode) {
            this.currentCutoff += this.solver.problem.variables.length;
            this.solver.head.control.restarts.measure = Enums.ERestartsMeasure.NODE;
            this.solver.restarter.currCutoff = this.currentCutoff;

            return true;
        }

        return false;
    }

    @Override
    public void afterRun() {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterRun();
        }

        if (this.samplingMode && this.solver.restarter.numRun == N_RUNS_FOR_SAMPLING - 1) {
            this.samplingMode = false;
            this.solver.head.control.restarts.measure = this.initialRestartMeasure;
            this.solver.restarter.reset();
        }
    }

    @Override
    public void whenWipeout(Constraint c, Variable x) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().whenWipeout(c, x);
        }
    }

    @Override
    public void beforeAssignment(Variable x, int a) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().beforeAssignment(x, a);
        }
    }

    @Override
    public void afterAssignment(Variable x, int a) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterAssignment(x, a);
        }
    }

    @Override
    public void afterUnassignment(Variable x) {
        for (VarHManager m : this.varHManagers) {
            m.getVarH().afterUnassignment(x);
        }
    }

    @Override
    public double scoreOf(Variable x) {
        return this.currentVarH.scoreOf(x);
    }

    @Override
    public Variable bestUnpriorityVar() {
        return this.currentVarH.bestUnpriorityVar();
    }

    @Override
    public String toString() {
        return this.currentVarHManager.toString();
    }

}
