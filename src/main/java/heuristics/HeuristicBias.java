package heuristics;

import constraints.Constraint;
import solver.Solver;
import variables.Variable;

import java.util.function.BiFunction;

public enum HeuristicBias {
    LEX((s, v) -> 0.),
    DEG((s, v) -> v.deg() * 1e-10),
    RAND((s, v) -> s.head.random.nextDouble() * 1e-10);

    private final BiFunction<Solver, Variable, Double> biasFunction;

    HeuristicBias(BiFunction<Solver, Variable, Double> biasFunction) {
        this.biasFunction = biasFunction;
    }

    public double apply(Solver s, Variable v) {
        return this.biasFunction.apply(s, v);
    }
}
