/**
 * AbsCon - Copyright (c) 2017, CRIL-CNRS - lecoutre@cril.fr
 * 
 * All rights reserved.
 * 
 * This program and the accompanying materials are made available under the terms of the CONTRAT DE LICENCE DE LOGICIEL LIBRE CeCILL which accompanies this
 * distribution, and is available at http://www.cecill.info
 */
package constraints.extension.structures;

import static org.xcsp.common.Constants.STAR;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.xcsp.common.Constants;
import org.xcsp.common.Range;
import org.xcsp.common.Utilities;
import org.xcsp.common.structures.Automaton;
import org.xcsp.common.structures.Transition;

import constraints.Constraint;
import constraints.extension.CMDD;
import utility.Kit;
import utility.Kit.IntArrayHashKey;
import variables.Domain;
import variables.Variable;

public final class MDD extends ExtensionStructure {

	/**********************************************************************************************
	 * MDDNode
	 *********************************************************************************************/

	public static final class MDDNode {

		public final static MDDNode nodeF = new MDDNode(null, 0); // with id = 0

		public final static MDDNode nodeT = new MDDNode(null, 1); // with id = 1

		public static int nBuiltNodes;

		private static boolean discardClassForNodeF = true; // hard coding

		/** The MDD to which belongs this node. */
		private final MDD mdd;

		/** The id of this node (must be unique) */
		public int id;

		/** The level of this node in the MDD to which it belongs */
		public final int level;

		/** All children (sons) of this node */
		public MDDNode[] sons;

		/**
		 * sonsClasses[i][j] is the index of the jth son in the ith equivalence class. Two indexes belong to the same class iff they reach the same child
		 */
		public int[][] sonsClasses;

		private Integer nSonsDifferentFromNodeF;

		/** Object used when building an MDD from an automaton or a KnapsSack; can be an Integer or a String */
		public Object state;

		public String name() {
			return this == nodeF ? "nodeF" : this == nodeT ? "nodeT" : level == 0 ? "root" : "n" + id;
		}

		public int nSonsDifferentFromNodeF() {
			return nSonsDifferentFromNodeF != null ? nSonsDifferentFromNodeF
					: (nSonsDifferentFromNodeF = (int) Stream.of(sons).filter(c -> c != nodeF).count());
		}

		public final boolean isLeaf() {
			return this == nodeF || this == nodeT;
		}

		private MDDNode(MDD mdd, int level) {
			this.mdd = mdd;
			if (mdd == null) {
				this.id = level;
				this.level = -1;
			} else {
				this.id = mdd.nextNodeId();
				this.level = level;
			}
		}

		public MDDNode(MDD mdd, int level, int nSons, boolean defaultNodeF) {
			this(mdd, level);
			this.sons = IntStream.range(0, nSons).mapToObj(i -> defaultNodeF ? nodeF : nodeT).toArray(MDDNode[]::new);
		}

		public MDDNode(MDD mdd, int level, int nSons, boolean defaultNodeF, Object state) {
			this(mdd, level, nSons, defaultNodeF);
			this.state = state;
		}

		private void addTuple(int level, int value, int[] tuple, boolean positive, int[] domSizes) {
			MDDNode son = sons[value];
			if (!son.isLeaf())
				son.addTuple(level + 1, tuple, positive, domSizes);
			else if (level == tuple.length - 1)
				sons[value] = positive ? nodeT : nodeF;
			else
				(sons[value] = new MDDNode(mdd, level + 1, domSizes[level + 1], positive)).addTuple(level + 1, tuple, positive, domSizes);
		}

		private void addTuple(int level, int[] tuple, boolean positive, int[] domSizes) {
			if (tuple[level] == Constants.STAR) {
				for (int i = 0; i < sons.length; i++)
					addTuple(level, i, tuple, positive, domSizes);
			} else
				addTuple(level, tuple[level], tuple, positive, domSizes);
		}

		public void addTuple(int[] tuple, boolean positive, int[] domSizes) {
			addTuple(0, tuple, positive, domSizes);
		}

		public void buildSonsClasses() {
			if (isLeaf() || sonsClasses != null)
				return; // already built
			Map<MDDNode, List<Integer>> map = IntStream.range(0, sons.length).filter(i -> !discardClassForNodeF || sons[i] != nodeF).boxed()
					.collect(Collectors.groupingBy(i -> sons[i]));
			sonsClasses = map.values().stream().map(list -> Kit.intArray(list)).toArray(int[][]::new);
			Stream.of(sons).forEach(s -> s.buildSonsClasses());
		}

		public int nInternalNodes(Set<Integer> set) {
			if (isLeaf() || set.contains(id))
				return 0; // static leaves are not counted here and nodes with id already in set are already counted
			set.add(id);
			return 1 + Stream.of(sons).mapToInt(c -> c.nInternalNodes(set)).sum();
		}

		private boolean canReachNodeT(Set<Integer> reachingNodes, Set<Integer> unreachingNodes) {
			if (this == nodeT || reachingNodes.contains(id))
				return true;
			if (this == nodeF || unreachingNodes.contains(id))
				return false;
			boolean found = false;
			for (int i = 0; i < sons.length; i++)
				if (!sons[i].canReachNodeT(reachingNodes, unreachingNodes))
					sons[i] = nodeF;
				else
					found = true;
			if (found)
				reachingNodes.add(id);
			else
				unreachingNodes.add(id);
			return found;
		}

		public boolean canReachNodeT() {
			return canReachNodeT(new HashSet<Integer>(), new HashSet<Integer>());
		}

		public int renameNodes(int lastId, Map<Integer, MDDNode> map) {
			if (isLeaf() || map.get(id) == this)
				return lastId;
			lastId++;
			map.put(id = lastId, this);
			for (MDDNode son : sons)
				lastId = son.renameNodes(lastId, map);
			// for (int i = 0; i < childClasses.length; i++) lastId = childs[childClasses[i][0]].renameNodes(lastId, map); // alternative
			return lastId;
		}

		public boolean controlUniqueNodes(Map<Integer, MDDNode> map) {
			MDDNode node = map.get(id);
			if (node == null)
				map.put(id, this);
			else
				Kit.control(node == this, () -> "two nodes with the same id in the MDD " + id);
			return sons == null || Stream.of(sons).noneMatch(child -> !child.controlUniqueNodes(map));
		}

		public void display(int[] cnts, boolean displayClasses) {
			if (this.isLeaf())
				return;
			Kit.log.fine(id + "@" + level + " => ");
			if (cnts != null)
				cnts[level]++;
			if (sons == null)
				return;
			Kit.log.fine("{" + Stream.of(sons).map(child -> child.id + "").collect(Collectors.joining(",")) + "}");
			if (displayClasses) {
				if (sonsClasses != null)
					for (int i = 0; i < sonsClasses.length; i++)
						Kit.log.fine("class " + i + " => {" + Kit.join(sonsClasses[i]) + "}");
				Kit.log.fine("nNotFFChilds=" + nSonsDifferentFromNodeF);
			}
			// if (similarChilds != null) for (int i = 0; i < similarChilds.length; i++)childs[similarChilds[i][0]].display(constraint, cnts);
			// else
			Stream.of(sons).filter(s -> s.id > id).forEach(s -> s.display(cnts, displayClasses));
		}

		public void display() {
			display(null, false);
		}

		public void displayTransitionsXCSP() {
			if (this.isLeaf())
				return;
			if (sons == null)
				return;
			System.out.print(IntStream.range(0, sons.length).filter(i -> sons[i] != nodeF).mapToObj(i -> "[\"q" + id + "\"," + i + ",\"q" + sons[i].id + "\"]")
					.collect(Collectors.joining(",")) + ",\n");
			Stream.of(sons).filter(s -> s.id > id).forEach(s -> s.displayTransitionsXCSP());
		}

		public int displayTuples(Domain[] doms, int[] currTuple, int currLevel, int cnt) {
			if (this == nodeT) { // && Kit.isArrayContainingValuesAllDifferent(currentTuple)) {
				Kit.log.info(Kit.join(currTuple));
				cnt++;
			}
			if (isLeaf())
				return cnt;
			for (int i = 0; i < sons.length; i++) {
				currTuple[currLevel] = doms[currLevel].toVal(i);
				cnt = sons[i].displayTuples(doms, currTuple, currLevel + 1, cnt);
			}
			return cnt;
		}

		private StringBuilder getTransitions(Domain[] doms, StringBuilder sb, Set<MDDNode> processedNodes) {
			if (sons != null) {
				for (int i = 0; i < sons.length; i++)
					if (sons[i] != nodeF)
						sb.append("(").append(name()).append(",").append(doms[level].toVal(i)).append(",").append(sons[i].name()).append(")");
				processedNodes.add(this);
				for (MDDNode son : sons)
					if (!processedNodes.contains(son))
						son.getTransitions(doms, sb, processedNodes);
			}
			return sb;
		}

		public String getTransitions(Domain[] doms) {
			return getTransitions(doms, new StringBuilder(), new HashSet<MDDNode>()).toString();
		}

		public void collectCompressedTuples(List<int[][]> list, int[][] t, int level) {
			if (this == nodeT)
				list.add(Kit.cloneDeeply(t));
			if (isLeaf())
				return;
			for (int i = 0; i < sonsClasses.length; i++) {
				t[level] = sonsClasses[i];
				MDDNode representativeChild = sons[sonsClasses[i][0]];
				representativeChild.collectCompressedTuples(list, t, level + 1);
			}
		}

		public MDDNode filter(int[][] values, int prevVal) {
			if (isLeaf())
				return this;
			// int left = -1;
			// for (int i = childs.length - 1; i >= 0; i--)
			// if (values[i] > prevVal && childs[i] != nodeF) {
			// left = i; break; }
			// MDDNode node = null;
			// if (left == -1) node = this;
			// else {
			MDDNode node = new MDDNode(mdd, level, sons.length, true);
			for (int i = 0; i < sons.length; i++)
				if (values[level][i] <= prevVal)
					node.sons[i] = sons[i];
			// }
			for (int i = 0; i < sons.length; i++)
				node.sons[i] = node.sons[i].filter(values, values[level][i]);
			return node;
		}
	}

	/**********************************************************************************************
	 * MDD
	 *********************************************************************************************/

	public MDDNode root;

	private Integer nNodes;

	private boolean reductionWhileProcessingTuples = false; // hard coding

	private int nCreatedNodes = 2; // because two two first pre-built nodes nodeF and nodeT

	public Integer nNodes() {
		return nNodes != null ? nNodes : (nNodes = 2 + root.nInternalNodes(new HashSet<Integer>()));
	}

	public int nextNodeId() {
		return nCreatedNodes++;
	}

	public MDD(CMDD c) {
		super(c);
	}

	public MDD(CMDD c, MDDNode root) {
		this(c);
		this.root = root;
	}

	public MDD(CMDD c, Automaton automata) {
		this(c);
		storeTuplesFromAutomata(automata, c.scp.length, Stream.of(c.scp).map(x -> x.dom).toArray(Domain[]::new));
	}

	public MDD(CMDD c, Transition[] transitions) {
		this(c);
		storeTuplesFromTransitions(transitions, Stream.of(c.scp).map(x -> x.dom).toArray(Domain[]::new));
	}

	public MDD(CMDD c, int[] coeffs, Object limits) {
		this(c);
		// Kit.control(Variable.haveSameDomainType(c.scp));
		// int[] values = IntStream.range(0, c.scp[0].dom.initSize()).map(i -> c.scp[0].dom.toVal(i)).toArray();
		storeTuplesFromKnapsack(coeffs, limits, Variable.initDomainValues(c.scp));
	}

	private MDDNode recursiveReduction(MDDNode node, Map<IntArrayHashKey, MDDNode> reductionMap) {
		if (node.isLeaf())
			return node;
		for (int i = 0; i < node.sons.length; i++)
			node.sons[i] = recursiveReduction(node.sons[i], reductionMap);
		IntArrayHashKey hk = new IntArrayHashKey(Stream.of(node.sons).mapToInt(c -> c.id).toArray());
		return reductionMap.computeIfAbsent(hk, k -> node);
	}

	private void reduce(int[] prevTuple, int[] currTuple, Map<IntArrayHashKey, MDDNode> reductionMap) {
		int i = 0;
		MDDNode node = root;
		while (prevTuple[i] == currTuple[i])
			node = node.sons[prevTuple[i++]];
		// assuming that tuples come in lex ordering, we can definitively reduce the left branch
		node.sons[prevTuple[i]] = recursiveReduction(node.sons[prevTuple[i]], reductionMap);
	}

	private void finalizeStoreTuples() {
		root.buildSonsClasses();
		nNodes = root.renameNodes(1, new HashMap<Integer, MDDNode>()) + 1;
		// System.out.println("MDD : nNodes=" + nNodes + " nBuiltNodes=" + nCreatedNodes);
		assert root.controlUniqueNodes(new HashMap<Integer, MDDNode>());
		// buildSplitter();

		// root.displayTransitionsXCSP();
		// root.display();
	}

	@Override
	public void storeTuples(int[][] tuples, boolean positive) {
		Kit.control(positive && tuples.length > 0);
		Constraint ctr = firstRegisteredCtr();
		int[] domainSizes = Variable.domSizeArrayOf(ctr.scp, true);
		Map<IntArrayHashKey, MDDNode> reductionMap = new HashMap<>(2000);
		this.root = new MDDNode(this, 0, domainSizes[0], positive);
		if (ctr.indexesMatchValues) {
			for (int i = 0; i < tuples.length; i++) {
				root.addTuple(tuples[i], positive, domainSizes);
				if (reductionWhileProcessingTuples && i > 0)
					reduce(tuples[i - 1], tuples[i], reductionMap);
			}
		} else {
			int[] previousTuple = null, currentTuple = new int[tuples[0].length];
			for (int[] tuple : tuples) {
				for (int i = 0; i < currentTuple.length; i++)
					currentTuple[i] = tuple[i] == STAR ? STAR : ctr.scp[i].dom.toIdx(tuple[i]);
				root.addTuple(currentTuple, positive, domainSizes);
				if (reductionWhileProcessingTuples) {
					if (previousTuple == null)
						previousTuple = currentTuple.clone();
					else {
						reduce(previousTuple, currentTuple, reductionMap);
						int[] tmp = previousTuple;
						previousTuple = currentTuple;
						currentTuple = tmp;
					}
				}
			}
			// constraint.setIndexValueSimilarity(true);
		}
		if (!reductionWhileProcessingTuples)
			recursiveReduction(root, reductionMap);
		finalizeStoreTuples();
	}

	public MDD storeTuplesFromTransitions(Transition[] transitions, Domain[] domains) {
		Map<String, MDDNode> nodes = new HashMap<>();
		Set<String> possibleRoots = new HashSet<>(), notRoots = new HashSet<>();
		Set<String> possibleWells = new HashSet<>(), notWells = new HashSet<>();
		for (Transition tr : transitions) {
			String src = tr.start, tgt = tr.end;
			notWells.add(src);
			notRoots.add(tgt);
			if (!notRoots.contains(src))
				possibleRoots.add(src);
			if (!notWells.contains(tgt))
				possibleWells.add(tgt);
			if (possibleRoots.contains(tgt))
				possibleRoots.remove(tgt);
			if (possibleWells.contains(src))
				possibleWells.remove(src);
		}
		Kit.control(possibleRoots.size() == 1 && possibleWells.size() == 1,
				() -> "sizes= " + possibleRoots.size() + " " + possibleWells.stream().collect(Collectors.joining(" ")));
		String sroot = possibleRoots.toArray(new String[1])[0];
		String swell = possibleWells.toArray(new String[1])[0];
		nodes.put(sroot, root = new MDDNode(this, 0, domains[0].initSize(), true));
		nodes.put(swell, MDDNode.nodeT);
		// TODO reordering transitions to guarantee that the src node has already been generated
		for (Transition tr : transitions) {
			MDDNode node1 = nodes.get(tr.start);
			long v = tr.value instanceof Integer ? (Integer) tr.value : (Long) tr.value;
			int val = Utilities.safeInt(v);
			int idx = domains[node1.level].toIdx(val);
			Kit.control(idx != -1);
			MDDNode node2 = nodes.computeIfAbsent((String) tr.end, k -> new MDDNode(this, node1.level + 1, domains[node1.level + 1].initSize(), true));
			// MDDNode node2 = nodes.get(tr[2]);
			// if (node2 == null)
			// nodes.put((String) tr[2], node2 = new MDDNode(this, node1.level + 1, domains[node1.level + 1].initSize(), true));
			node1.sons[idx] = node2;
		}
		// root.canReachNodeT();
		finalizeStoreTuples();
		return this;
	}

	private Map<String, List<Transition>> buildNextTransitions(Automaton automata) {
		Map<String, List<Transition>> map = new HashMap<>();
		map.put(automata.startState, new ArrayList<>());
		Stream.of(automata.finalStates).forEach(s -> map.put(s, new ArrayList<>()));
		Stream.of(automata.transitions).forEach(t -> {
			map.put(t.start, new ArrayList<>());
			map.put(t.end, new ArrayList<>());
		});
		Stream.of(automata.transitions).forEach(t -> map.get(t.start).add(t));
		return map;
	}

	public MDD storeTuplesFromAutomata(Automaton automata, int arity, Domain[] domains) {
		Kit.control(arity > 1 && IntStream.range(1, domains.length).allMatch(i -> domains[i].typeIdentifier() == domains[0].typeIdentifier()));
		Map<String, List<Transition>> nextTrs = buildNextTransitions(automata);
		this.root = new MDDNode(this, 0, domains[0].initSize(), true, automata.startState);
		Map<String, MDDNode> prevs = new HashMap<>(), nexts = new HashMap<>();
		prevs.put((String) root.state, root);
		for (int i = 0; i < arity; i++) {
			for (MDDNode node : prevs.values()) {
				for (Transition tr : nextTrs.get(node.state)) {
					int v = Utilities.safeInt((Long) tr.value);
					int a = domains[i].toIdx(v);
					if (a != -1) {
						String nextState = tr.end;
						if (i == arity - 1) {
							node.sons[a] = Utilities.indexOf(nextState, automata.finalStates) != -1 ? MDDNode.nodeT : MDDNode.nodeF;
						} else {
							// MDDNode nextNode = nexts.computeIfAbsent(nextState, k -> new MDDNode(this, i + 1, domains[i].initSize(), true,
							// nextState)); // pb with i not final
							MDDNode nextNode = nexts.get(nextState);
							if (nextNode == null)
								nexts.put(nextState, nextNode = new MDDNode(this, i + 1, domains[i].initSize(), true, nextState));
							node.sons[a] = nextNode;
						}
					}
				}
			}
			Map<String, MDDNode> tmp = prevs;
			prevs = nexts;
			nexts = tmp;
			nexts.clear();
		}
		root.canReachNodeT();
		finalizeStoreTuples();
		return this;
	}

	// coeffs, and limits (either a Range or an int array) from the knapsack constraint, and values are the possible values at each level
	public MDD storeTuplesFromKnapsack(int[] coeffs, Object limits, int[][] values) {
		this.root = new MDDNode(this, 0, values[0].length, true, 0);
		Map<Integer, MDDNode> prevs = new HashMap<>(), nexts = new HashMap<>();
		prevs.put((Integer) root.state, root);
		for (int level = 0; level < coeffs.length; level++) {
			for (MDDNode node : prevs.values()) {
				for (int j = 0; j < values[level].length; j++) {
					int nextState = (Integer) node.state + coeffs[level] * values[level][j];
					if (level == coeffs.length - 1) {
						if (limits instanceof Range)
							node.sons[j] = ((Range) limits).contains(nextState) ? MDDNode.nodeT : MDDNode.nodeF;
						else
							node.sons[j] = Utilities.indexOf(nextState, (int[]) limits) != -1 ? MDDNode.nodeT : MDDNode.nodeF;
					} else {
						MDDNode nextNode = nexts.get(nextState);
						if (nextNode == null)
							nexts.put(nextState, nextNode = new MDDNode(this, level + 1, values[level + 1].length, true, nextState));
						node.sons[j] = nextNode;
					}
				}
			}
			Map<Integer, MDDNode> tmp = prevs;
			prevs = nexts;
			nexts = tmp;
			nexts.clear();
		}
		root.canReachNodeT();

		boolean increasing = false;
		if (!increasing) {
			finalizeStoreTuples();
			// displayTuples();
		} else {
			root = root.filter(values, Integer.MAX_VALUE);
			recursiveReduction(root, new HashMap<>(2000));
			finalizeStoreTuples();
			root.display();
			displayTuples();
		}
		return this;
	}

	@Override
	public boolean checkIdxs(int[] t) {
		MDDNode node = root;
		for (int i = 0; !node.isLeaf(); i++)
			node = node.sons[t[i]];
		return node == MDDNode.nodeT;
	}

	public void displayTuples() {
		int cnt = root.displayTuples(Variable.buildDomainsArrayFor(firstRegisteredCtr().scp), new int[firstRegisteredCtr().scp.length], 0, 0);
		Kit.log.info(" => " + cnt + " tuples");
	}

	/**********************************************************************************************
	 * Start of experimental section (splitting - compression)
	 *********************************************************************************************/

	private boolean mustBuildSplitter = false;

	private MDDSplitter splitter;

	public MDDSplitter getSplitter() {
		return splitter;
	}

	void buildSplitter() {
		if (mustBuildSplitter) {
			int arity = firstRegisteredCtr().scp.length;
			splitter = new MDDSplitter(this, new int[] { (int) Math.ceil(arity / 2.0), (int) Math.floor(arity / 2.0) });
		}
	}

	public int[][][] buildCompressedTable() {
		// root.buildChildClasses();
		Constraint ctr = firstRegisteredCtr();
		LinkedList<int[][]> list = new LinkedList<>();
		root.collectCompressedTuples(list, new int[ctr.scp.length][], 0);
		int[][][] compressedTuples = Kit.intArray3D(list);
		return compressedTuples;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		int length = Integer.parseInt(args[0]);
		// buildFromAutomata(2, new int[][] { { 1, 2 }, { -1, 3 }, { 3, -1 }, { 3, 4 }, { -1, -1 } }, 0, new int[] { 4 }, 5);
		// Automata automata = new Automata(10, 2, new int[][] { { 0, 0, 1 }, { 0, 1, 2 }, { 1, 1, 3 }, { 2, 0, 3 }, { 3, 0, 3 }, { 3, 1, 4
		// } }, 0, new int[] {
		// 4 });
		// Automata automata = null;
		// automata = new Automata(10, 3, new int[][] { { 0, 0, 1 }, { 0, 2, 2 }, { 1, 0, 3 }, { 2, 2, 4 }, { 3, 0, 5 }, { 3, 1, 7 }, { 4,
		// 1, 7 }, { 4, 2, 6 },
		// { 5, 1, 7 }, { 6, 1, 7 },
		// { 7, 1, 8 }, { 8, 0, 1 }, { 8, 1, 9 }, { 8, 2, 2 }, { 9, 0, 1 }, { 9, 2, 2 } }, 0, new int[] { 3, 4, 5, 6, 8, 9 });
		// automata = new Automata(7, 4, new int[][] { { 0, 0, 0 }, { 0, 1, 1 }, { 0, 2, 2 }, { 0, 3, 3 }, { 1, 0, 4 }, { 1, 1, 1 }, { 2, 0,
		// 5 }, { 2, 2, 2 }, {
		// 3, 0, 6 }, { 3, 3, 3 },
		// { 4, 0, 4 }, { 4, 1, 1 }, {4,2,2}, { 5, 0, 5 }, { 5, 2, 2 }, {5,3,3}, { 6, 0, 6 }, { 6, 3, 3 } ,{6,1,1}}, 0, new int[] { 0, 1, 2,
		// 3, 4, 5,6 });

		// automata = new Automata(5, 2, new int[][] { { 0, 0, 0 }, { 0, 1, 1 }, { 1, 1, 2 }, { 2, 0, 3 }, { 3, 0, 3 }, { 3, 1, 4 }, { 4, 0,
		// 4 } }, 0, new int[]
		// { 4 });
		// MDD m = new MDD(null).storeTuples(automata, length);

		// MDD m = new MDD(null).storeTuplesFromKnapsack(70,82,new int[] {27,37,45,53} , new int[] {0,1,2,3});
		// MDD m = new MDD(null).storeTuplesFromKnapsack(34,34, Kit.buildIntArrayWithUniqueValue(4, 1) , Kit.buildIntArray(16, 1));
		// MDD m = new MDD(null).storeTuplesFromKnapsack(65,65, Kit.buildIntArrayWithUniqueValue(5, 1) , Kit.buildIntArray(25, 1));
		// MDD m = new MDD(null).storeTuplesFromKnapsack(111,111, Kit.buildIntArrayWithUniqueValue(6, 1) , Kit.buildIntArray(36, 1));
		// MDD m = new MDD(null).storeTuplesFromKnapsack(175,175, Kit.buildIntArrayWithUniqueValue(7, 1) , Kit.buildIntArray(49, 1));
		// MDD m = new MDD(null).storeTuplesFromKnapsack(505,505, Kit.buildIntArrayWithUniqueValue(10, 1) , Kit.buildIntArray(100, 1));
		// MDD m = new MDD(null).storeTuplesFromKnapsack(1379,1379, Kit.buildIntArrayWithUniqueValue(14, 1) , Kit.buildIntArray(196, 1));
		// MDD m = new MDD(null).storeTuplesFromKnapsack(Kit.repeat(1, 20), new Range(4010, 4010), Kit.range(1, 400));
		// m.root.display();
		// Kit.prn(" => " + m.root.displayTuples(new int[length], 0, 0) + " tuples");
	}
}

// if (!directEncodingFromAutomata) buildSubtree root.buildSubtree(nbLetters, transitions, finalStates, height + 1, 0, new
// HashMap<IntArrayHashKey,
// MDDNode>(2000));

// public static void storeTuples1(int nbStates, int nbLetters, int[][] transitions, int initialState, int[] finalStates, int nbLevels) {
// Kit.control(nbLevels > 1);
// Map<Integer, MDDNode> map = new HashMap<Integer, MDDNode>();
// MDDNode root = new MDDNode(this,0, false, nbLetters); // TODO a virer la declaertaionxxxxxxxxxxxxxxxxxxxxxxxxxxx
// List<MDDNode> listOfNodesAtCurrentLevel = new ArrayList<MDDNode>(), nextList = new ArrayList<MDDNode>();
// listOfNodesAtCurrentLevel.add(root);
// root.state = initialState;
// for (int level = 0; level < nbLevels - 1; level++) {
// nextList.clear();
// for (MDDNode node : listOfNodesAtCurrentLevel) {
// map.clear();
// for (int letter = 0; letter < nbLetters; letter++) {
// int nextState = transitions[node.state][letter];
// if (nextState == -1)
// continue;
// if (level == nbLevels - 2) {
// if (Kit.isArrayContaining(finalStates, nextState)) {
// node.setChild(letter, MDDNode.nodeTT);
// }
// continue;
// }
// MDDNode nextNode = map.get(nextState);
// if (nextNode == null) {
// nextNode = new MDDNode(this,level + 1, true, nbLetters);
// nextNode.state = nextState;
// map.put(nextState, nextNode);
// nextList.add(nextNode);
// }
// node.setChild(letter, nextNode);
// }
// }
// List<MDDNode> tmp = listOfNodesAtCurrentLevel;
// listOfNodesAtCurrentLevel = nextList;
// nextList = tmp;
// }
// root.display(null);
// }

// private static boolean buildSubtree(MDDNode node, int nbLetters, int[][] transitions, int[] finalStates, int nbLevels, int level) {
// boolean found = false;
// Map<Integer, MDDNode> map = new HashMap<Integer, MDDNode>();
// for (int letter = 0; letter < nbLetters; letter++) {
// int nextState = transitions[node.state][letter];
//
// boolean trueNode = false;
// if (level == nbLevels - 2) {
// trueNode = nextState != -1 && Kit.isArrayContaining(finalStates, nextState);
// node.setChild(letter, trueNode ? MDDNode.nodeTT : MDDNode.nodeFF);
// } else {
// MDDNode nextNode = map.get(nextState);
// if (nextNode == null) {
// nextNode = new MDDNode(this,level + 1, true, nbLetters);
// nextNode.state = nextState;
// map.put(nextState, nextNode);
// }
// trueNode = nextState != -1 && buildSubtree(nextNode, nbLetters, transitions, finalStates, nbLevels, level + 1);
// node.setChild(letter, trueNode ? nextNode : MDDNode.nodeFF);
// }
//
// found = found || trueNode;
// }
//
//
// /*
// * MDDNode[] childs = node.getChilds(); int[] t = new int[childs.length]; for (int i = 0; i < childs.length; i++) t[i] =
// childs[i].getId(); IntArrayHashKey hk
// = new IntArrayHashKey(t); MDDNode
// identicalNode =
// * reductionMapBisTmp.get(hk); if (identicalNode == null) { reductionMapBisTmp.put(hk, node); return node; } else return identicalNode;
// */
//
// return found;
// }
